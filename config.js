{
	"port" : 8088,
	"prefix" : '/geo',
	"url": "tcp://user:pass@host:port/dbname",

	"tables": {
	  "things" : { "geom":"geom", "cols":["id", "owner", "date"] },
	  "people" : { "geom":"point",   "cols":["id", "firstname", "lastname", "email" ] },
	  "places" : { "geom":"latlong",   "cols":["id", "name", "type" ] }
	}
}