SELECT 
		row_to_json(fc) 
FROM ( 
		SELECT 
			'FeatureCollection' As type, 
			row_to_json( ( 	SELECT 
								l 
							FROM (
									SELECT 
										'name' as type, 
										row_to_json( ( 	SELECT 
															l2 
														FROM ( 
															SELECT 'urn:ogc:def:crs:EPSG::'||Find_SRID('public', '@TABLE@', '@GEOM@') as name
														) as l2
													) ) as properties 
								) As l
						) ) as crs, 
			array_to_json( array_agg(f) ) As features 
		FROM (
				SELECT 'Feature' As type 
				, ST_AsGeoJSON(lg.@GEOM@, 0)::json As geometry 
				, row_to_json( ( 	SELECT 
										l 
									FROM ( 	SELECT @PROPS@ ) As l 
							)) As properties 
				FROM 
					@TABLE@ As lg 
				WHERE @CLAUSE@ 
			) As f 
	)  As fc;