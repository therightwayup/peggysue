# Peggy Sue

Licensed under the ISC License.

...is a simple REST endpoint for retriveing GeoJSON representations of PostGIS geometries (and other columns as properties)

It is based on the excellent SQL snippet published here: http://www.postgresonline.com/journal/archives/267-Creating-GeoJSON-Feature-Collections-with-JSON-and-PostGIS-functions.html 
and expanded to add CRS directly from the geometry column.

## Install

Make a config.js files to suit you database and contents.  

 - At present a database role with SELECT only privileges would be best.
 - Edit the URL
 - change the prefix to something starting with /, or keep /geo as the default
 - Describe your tables: 
  - table name as key
  - which col is the geometry
  - which columns to export as JSON properties.

## Usage

GET http://host:port/${prefix}/prop/<table>/<field_to_query>/<query_value>

E.g. GET http://localhost:8088/geo/prop/things/id/1

will return in GeoJSON the row from the 'things' database that has "id" == 1