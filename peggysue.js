// npm install restify pg
// query from: // from http://www.postgresonline.com/journal/archives/267-Creating-GeoJSON-Feature-Collections-with-JSON-and-PostGIS-functions.html

////////////////////////////////////////

var restify = require('restify');
var pg = require("pg");
var jf = require("jsonfile");
var fs = require("fs");

var cfgFile = process.argv[2] ? process.argv[2] : "config.js"
console.log("reading config from: " + cfgFile);
var cfg = jf.readFileSync(cfgFile); // TODO check
var sql = fs.readFileSync("base.sql", { encoding:'UTF-8' })// TODO check

function byProp(req, res, next){

    var vals = [];
    var prop = req.params.prop;
    if( ! testSQLPropOK(prop) ){
        console.log("Prop \"" + prop + "\" Seems unsafe");
        return next(new restify.InvalidArgumentError("I just don't like -->" + prop + "<--"));
    }

    var clause = "\"" + prop + "\" = $1" // TODO prevent injections
    vals.push(req.params.id);

    return execute(req,res,next,clause,vals);
}

function execute(req, res, next, clause, vals) {

    var log = req.log;
    var table = req.params.table;

    // check table exists
    var tableCfg = cfg.tables[table];

    if( typeof tableCfg === 'undefined' ){
        return next( new restify.InvalidArgumentError("I just don't like table with name -->" + req.params.table + "<--") );
    }

    var cols2props = tableCfg.cols.join(",");
    var geom = tableCfg.geom;

    pg.connect(cfg.url, function(err, client) {

        var query = sql.replace(/@TABLE@/g, table)
                        .replace(/@GEOM@/g, geom)
                        .replace(/@PROPS@/g, cols2props)
                        .replace(/@CLAUSE@/g, !!(clause) ? clause : "1" );

        client.query(query, vals, function(err, result) {
            res.send(result.rows[0].row_to_json);
            client.end();
            return next();
        });
    });

}

var server = restify.createServer();

server.use(restify.CORS());

//server.get('/geo/:table', execute);
server.get( cfg.prefix + '/prop/:table/:prop/:id', byProp);
//server.get('/geo/bbox/:table/:sw/:se', byBoundingBox);

server.listen(cfg.port, function() {
  console.log('%s listening at %s', server.name, server.url);
});

server.on('uncaughtException', function (req, res, route, err) {
    console.error(err.stack);
    res.send(err)
    //process.exit(1);
});

///////////////////// Helpers ////////////////////////////

/**
 * check if the supplied SQL property is OK to be used as part of string concat.
 *
 * just checks for OK chars, will return false if anything like " or ; are present
 */
function testSQLPropOK(sqlProperty){
    return sqlProperty.match( /^[a-zA-Z0-9-_:+]+$/ )
}